@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" aria-describedby="emailHelp">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Umur</label>
        <input type="text" class="form-control" id="umur" name="umur" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea id="bio" name="bio" class="form-control" col="10" row="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection