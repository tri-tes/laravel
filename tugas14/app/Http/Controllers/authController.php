<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
        // dd($request->all());
        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];
        
        return view('page.welcome', ["namaDepan"=>$namaDepan,"namaBelakang"=>$namaBelakang]);
    }
}

