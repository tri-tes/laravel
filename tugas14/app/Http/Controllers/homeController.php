<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function dashboard()
    {
        return view('page.home');
    }

    public function welcome()
    {
        return view('welcome');
    }
}
