<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\authController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [homeController::class, 'dashboard'] );

Route::get('/', [homeController::class, 'welcome'] );

Route::get('/register', [authController::class, 'register'] );

Route::post('/kirim', [authController::class, 'kirim'] );

// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.datatable');
});