@extends('layout.master')

@section('judul')
    Halaman List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $value)
          <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $value->nama }}</td>
            <td>
                <a href="" class="btn btn-info btn-sm">Detail</a>
            </td>
          </tr>
      @empty
          <tr>Tidak ada data</tr>
      @endforelse
    </tbody>
  </table>
@endsection